package eu.datavaults

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DataModelTest {

    @BeforeAll
    fun `Load and initialize vocabularies`() {
        init()
    }

    @Test
    fun `All iso codes of countries`() {
        assertFalse { Countries.isoCodes.isEmpty() }
    }

    @Test
    fun `All identifiers of countries`() {
        assertFalse { Countries.identifiers.isEmpty() }
    }

    @Test
    fun `Get ISO alpha 2 code of country`() {
        val ger = Countries.fromIdentifier("DEU")
        assertNotNull(ger)
        assertTrue(Countries.iso31661alpha2(ger) == "DE")
    }

    @Test
    fun `Get country by name`() {
        val ger = Countries.fromLabel("Allemagne", "fr")
        assertNotNull(ger)
        assertTrue(Countries.iso31661alpha2(ger) == "DE")
    }

    @Test
    fun `Get country by iso code`() {
        val fr = Countries.fromIso31661Alpha2Code("FR")
        assertNotNull(fr)
        assertEquals("France", fr.label("en"))
    }

    @Test
    fun `All identifiers of places`() {
        assertFalse { Places.identifiers.isEmpty() }
    }

    @Test
    fun `Get place by name`() {
        val berlin = Places.fromLabel("Berlin", "fr")
        assertNotNull(berlin)
    }

    @Test
    fun `Get country of place`() {
        val berlin = Places.fromLabel("Berlin", "fr")
        assertNotNull(berlin)
        val country = Places.countryOf(berlin)
        assertNotNull(country)
        assertTrue(country.identifier == "DEU")
    }

}
