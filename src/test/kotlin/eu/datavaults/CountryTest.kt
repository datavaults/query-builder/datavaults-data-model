package eu.datavaults

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class CountryTest {

    @Test
    fun `Find country by name`() {
        assertNotNull(Country.fromName("Germany"))
        assertNotNull(Country.fromName("GERMANY"))
        assertNotNull(Country.fromName("germany"))
        assertNotNull(Country.fromName("gerMANY"))
    }

    @Test
    fun `Find country by id`() {
        assertNotNull(Country.fromIdentifier("DEU"))
        assertNotNull(Country.fromIdentifier("deu"))
        assertNotNull(Country.fromIdentifier("Deu"))
        assertNotNull(Country.fromIdentifier("deU"))
    }

    @Test
    fun `Find country by iso code`() {
        assertNotNull(Country.fromIsoCode("de"))
        assertNotNull(Country.fromIsoCode("DE"))
        assertNotNull(Country.fromIsoCode("De"))
        assertNotNull(Country.fromIsoCode("dE"))
    }

    @Test
    fun `Check country properties`() {
        val germany = Country.fromUriRef("http://publications.europa.eu/resource/authority/country/DEU")
        assertNotNull(germany)
        assertEquals("Germany", germany.name)
        assertEquals("Allemagne", germany.name("Fr"))
        assertEquals("Allemagne", germany.names["fr"])
        assertEquals("DEU", germany.identifier)
        assertEquals("DE", germany.isoCode)
    }

}