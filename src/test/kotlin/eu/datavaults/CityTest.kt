package eu.datavaults

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class CityTest {

    @Test
    fun `Find city`() {
        assertNotNull(City.fromName("London"))
        assertNotNull(City.fromName("LONDON"))
        assertNotNull(City.fromName("london"))
        assertNotNull(City.fromName("lonDON"))
    }

    @Test
    fun `Get country of city`() {
        val city = City.fromName("London")
        assertNotNull(city)
        assertEquals("United Kingdom", city.country?.name)
    }

    @Test
    fun `Check city properties`() {
        val berlin = City.fromUriRef("http://publications.europa.eu/resource/authority/place/DEU_BER")
        assertNotNull(berlin)
        assertEquals("Berlin", berlin.name)
        assertEquals("Berlino", berlin.name("It"))
        assertEquals("Berlin", berlin.names["fr"])
        assertEquals("DEU_BER", berlin.identifier)
    }

}