package eu.datavaults.rdf

import eu.datavaults.titleCase
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFFormat
import org.apache.jena.riot.RDFFormatVariant
import org.junit.jupiter.api.Test

class IndividualTest {

    @Test
    fun `Build individual`() {

        val testJson = "{\"region\":\"Liverpool\",\"city\":\"Liverpool\",\"country\":\"United Kingdom\",\"placeOfBirth\":\"Liverpool\",\"nationality\":\"United Kingdom\",\"requestResolverSettings\":\"true\",\"occupations\":[2],\"culturalInterests\":[3,4],\"transportationMeans\":[3],\"nationalInsuranceNumber\":\"12345\",\"demonstrator\":\"miwenergia\"}"

        val jsonString = """
            {
                "region": "Attiki",
                "city":  "Patras",
                "country": "Greece",
                "requestResolverSettings": "true",
                "occupations": [
                  1
                ],
                "educations": [
                  1,
                  3
                ],
                "culturalInterests": [
                  1
                ],
                "transportationMeans": [
                  2
                ],
                "civilStatus": [
                  1
                ],
                "disabilities": [
                  1
                ],
                "demonstrator": "miwenergia"
            }
        """.trimIndent()

        val individual = DataVaults.buildIndividual("sherlock-holmes", testJson)

        println(individual.asRdfString())
        println(individual.extractJson())
    }

}