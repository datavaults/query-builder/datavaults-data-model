package eu.datavaults

class License private constructor(val concept: Concept) {

    val uriRef
        get() = concept.resource.uri

    val identifier
        get() = concept.identifier

    val name = concept.label("en")

    fun name(lang: String) = concept.label(lang.lowercase())

    val names = concept.prefLabels

    companion object {
        @JvmStatic
        fun fromUriRef(uriRef: String) = Places.getConcept(uriRef)?.let {
            License(it)
        }

        @JvmStatic
        fun fromIdentifier(identifier: String) = Licenses.fromIdentifier(identifier.uppercase())?.let {
            License(it)
        }

        @JvmStatic
        @JvmOverloads
        fun fromName(name: String, lang: String = "en") = Licenses.fromLabel(name.titleCase(), lang)?.let {
            License(it)
        }
    }

}