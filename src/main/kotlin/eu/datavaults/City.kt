package eu.datavaults

class City private constructor(val concept: Concept) {

    val uriRef
        get() = concept.resource.uri

    val identifier
        get() = concept.identifier

    val name = concept.label("en")

    fun name(lang: String) = concept.label(lang.lowercase())

    val names = concept.prefLabels

    val country
        get() = Places.countryOf(concept)?.let { Country(it) }

    companion object {
        @JvmStatic
        fun fromUriRef(uriRef: String) = Places.getConcept(uriRef)?.let {
            City(it)
        }

        @JvmStatic
        fun fromIdentifier(identifier: String) = Places.fromIdentifier(identifier.uppercase())?.let {
            City(it)
        }

        @JvmStatic
        @JvmOverloads
        fun fromName(name: String, lang: String = "en") = Places.fromLabel(name.titleCase(), lang)?.let {
            City(it)
        }
    }

}