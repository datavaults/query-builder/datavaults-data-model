package eu.datavaults

class Country internal constructor(val concept: Concept) {

    val uriRef
        get() = concept.resource.uri

    val identifier
        get() = concept.identifier

    val isoCode
        get() = Countries.iso31661alpha2(concept)

    val name = concept.label("en")

    fun name(lang: String) = concept.label(lang.lowercase())

    val names = concept.prefLabels

    companion object {
        @JvmStatic
        fun fromUriRef(uriRef: String) = Countries.getConcept(uriRef)?.let {
            Country(it)
        }

        @JvmStatic
        fun fromIdentifier(identifier: String) = Countries.fromIdentifier(identifier.uppercase())?.let {
            Country(it)
        }

        @JvmStatic
        @JvmOverloads
        fun fromName(name: String, lang: String = "en") = Countries.fromLabel(name.titleCase(), lang.lowercase())?.let {
            Country(it)
        }

        @JvmStatic
        fun fromIsoCode(isoCode: String) = Countries.fromIso31661Alpha2Code(isoCode.uppercase())?.let {
            Country(it)
        }
    }

}
