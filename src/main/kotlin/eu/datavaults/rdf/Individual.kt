package eu.datavaults.rdf

import eu.datavaults.Countries
import eu.datavaults.Country
import eu.datavaults.Place
import eu.datavaults.Places
import io.vertx.core.json.JsonObject
import org.apache.jena.datatypes.xsd.XSDDatatype
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.RDF
import org.apache.jena.vocabulary.VCARD4
import java.io.StringWriter

class Individual(private val model: Model) {
    @JvmOverloads
    fun asRdfString(format: RDFFormat = RDFFormat.RDFXML_ABBREV) = StringWriter().also {
        RDFDataMgr.write(it, model, format)
    }.toString()

    fun extractJson(): String {
        val json = JsonObject()
        model.listSubjectsWithProperty(RDF.type, DCAT.Catalog).nextOptional().ifPresent { individual ->
            individual.listProperties().forEach { (_, predicate, obj) ->
                when (predicate) {
                    DCTerms.publisher -> {
                        val publisher = obj.asResource()
                        publisher.listProperties().forEach { (_, p, o) ->
                            when (p) {
                                FOAF.firstName -> json.put("firstName", o.asLiteral().lexicalForm)
                                FOAF.lastName -> json.put("lastName", o.asLiteral().lexicalForm)
                                FOAF.gender -> json.put("gender", o.asLiteral().lexicalForm)
                                FOAF.birthday -> json.put("birthday", o.asLiteral().lexicalForm)
                                DVDM.placeOfBirth -> json.putIfNotNull(
                                    "placeOfBirth",
                                    Places.getConcept(o.asResource())?.prefLabel("en")
                                )
                                DVDM.nationality -> json.putIfNotNull(
                                    "nationality",
                                    Countries.getConcept(o.asResource())?.prefLabel("en")
                                )
                            }
                        }
                    }

                    DCAT.contactPoint -> {
                        val contactPoint = obj.asResource()
                        contactPoint.listProperties().forEach { (_, p, o) ->
                            when (p) {
                                VCARD4.hasAddress -> {
                                    json.putIfNotNull("postalCode", o.asResource().getProperty(VCARD4.postal_code))
                                }
                            }
                        }
                    }

                    DCTerms.spatial -> json.putIfNotNull(
                        "country",
                        Countries.getConcept(obj.asResource())?.prefLabel("en")
                    )

                    DVDM.city -> json.putIfNotNull("city", Places.getConcept(obj.asResource())?.prefLabel("en"))
                    DVDM.region -> json.putIfNotNull("region", obj.asLiteral().lexicalForm)

                    DVDM.occupation -> json.withJsonArray("occupations").add(obj.asLiteral().int)
                    DVDM.qualification -> json.withJsonArray("qualifications").add(obj.asLiteral().int)
                    DVDM.education -> json.withJsonArray("educations").add(obj.asLiteral().int)
                    DVDM.transportation -> json.withJsonArray("transportationMeans").add(obj.asLiteral().int)
                    DVDM.culturalInterest -> json.withJsonArray("culturalInterests").add(obj.asLiteral().int)

                    DVDM.civilStatus -> json.withJsonArray("civilStatus").add(obj.asLiteral().int)

                    DVDM.disability -> json.withJsonArray("disabilities").add(obj.asLiteral().int)

                    DVDM.nationalInsuranceNr -> json.put("nationalInsuranceNr", obj.asLiteral().lexicalForm)
                    DVDM.memberNumber -> json.put("memberNumber", obj.asLiteral().lexicalForm)
                    DVDM.socialSecurityNumber -> json.put("socialSecurityNumber", obj.asLiteral().lexicalForm)


                }
            }
        }
        return json.encodePrettily()
    }

    companion object {
        @JvmStatic
        @JvmOverloads
        fun fromRdfString(content: String, format: RDFFormat = RDFFormat.RDFXML): Individual {
            val model = ModelFactory.createDefaultModel()
            RDFDataMgr.read(model, content.byteInputStream(), format.lang)
            return Individual(model)
        }
    }
}
