package eu.datavaults.rdf

import eu.datavaults.Country
import eu.datavaults.License
import eu.datavaults.Place
import eu.datavaults.vocabularies.setNsPrefixesFiltered
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.apache.jena.datatypes.xsd.XSDDatatype
import org.apache.jena.rdf.model.*
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFFormat
import org.apache.jena.riot.RDFParser
import org.apache.jena.riot.system.Prefixes
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.util.ResourceUtils
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.RDF
import org.apache.jena.vocabulary.VCARD4

const val DM_NS = "http://datavaults.eu/"

class DataVaults {

    companion object {
        @JvmStatic
        fun buildIndividual(id: String, jsonString: String): Individual {
            val json = JsonObject(jsonString)

            val model = ModelFactory.createDefaultModel()
            val catalogue = model.createResource("${DM_NS}id/individual/$id", DCAT.Catalog)
            val publisher = model.createResource(FOAF.Person)
            val contactPoint = model.createResource(VCARD4.Individual)

            catalogue.addProperty(DCTerms.publisher, publisher)
            catalogue.addProperty(DCAT.contactPoint, contactPoint)

            catalogue.addProperty(DCTerms.title, id)

            json.forEach { (property, value) ->
                when (property) {
                    // Publisher properties
                    "firstName" -> publisher.addProperty(FOAF.firstName, value as String)
                    "lastName" -> publisher.addProperty(FOAF.lastName, value as String)
                    "gender" -> publisher.addProperty(FOAF.gender, value as String)
                    "birthday" -> publisher.addProperty(FOAF.birthday, value as String, XSDDatatype.XSDdate)
                    "placeOfBirth" -> Place.fromName(value as String)?.let {
                        publisher.addProperty(DVDM.placeOfBirth, it.concept.resource)
                    }

                    "nationality" -> Country.fromName(value as String)?.let {
                        publisher.addProperty(DVDM.nationality, it.concept.resource)
                    }

                    // Contact point properties
                    "postalCode" -> {}

                    // Catalogue properties
                    "country" -> Country.fromName(value as String)?.let {
                        catalogue.addProperty(DCTerms.spatial, it.concept.resource)
                    }

                    "city" -> Place.fromName(value as String)?.let {
                        catalogue.addProperty(DVDM.city, it.concept.resource)
                    }

                    "region" -> catalogue.addProperty(DVDM.region, value as String)

                    "occupations" -> catalogue.addLongArray(value, DVDM.occupation)
                    "qualifications" -> catalogue.addLongArray(value, DVDM.qualification)
                    "educations" -> catalogue.addLongArray(value, DVDM.education)
                    "transportationMeans" -> catalogue.addLongArray(value, DVDM.transportation)
                    "culturalInterests" -> catalogue.addLongArray(value, DVDM.culturalInterest)
                    "civilStatus" -> catalogue.addLongArray(value, DVDM.civilStatus)
                    "disabilities" -> catalogue.addLongArray(value, DVDM.disability)
                    "nationalInsuranceNumber" -> catalogue.addLiteral(DVDM.nationalInsuranceNr, value as String)
                    "memberNumber" -> catalogue.addLiteral(DVDM.memberNumber, value as String)
                    "socialSecurityNumber" -> catalogue.addLiteral(DVDM.socialSecurityNumber, value as String)
                }
            }

            setNsPrefixesFiltered(model)
            return Individual(model)
        }

        @JvmStatic
        @JvmOverloads
        fun buildAsset(id: String, jsonString: String, jsonldString: String? = null): Asset {
            val json = JsonObject(jsonString)

            val metadata = if (json.containsKey("metadata")) {
                (json.remove("metadata") as JsonObject).encode()
            } else ""

            val model = ModelFactory.createDefaultModel()
            val assetString = jsonldString ?: metadata

            RDFParser.create()
                .checking(false)
                .source(assetString.byteInputStream())
                .lang(Lang.JSONLD)
                .parse(model)

            val dataset = model.listResourcesWithProperty(RDF.type, DCAT.Dataset).toList()
                .firstOrNull()?.rename("${DM_NS}set/data/$id")
                ?: model.createResource("${DM_NS}set/data/$id", DCAT.Dataset)

            json.forEach { (property, value) ->
                when (property) {
                    "category" -> dataset.addStringArray(value, DCTerms.type)
                    "name" -> dataset.addLangString(value, DCTerms.title)
                    "description" -> dataset.addLangString(value, DCTerms.description)
                    "keywords" -> dataset.addStringArray(value, DCAT.keyword)
                    "license" -> License.fromIdentifier(value as String)?.let {
                        dataset.addProperty(DCTerms.license, it.concept.resource)
                    }

                    "price" -> dataset.addLiteral(DVDM.price, value)
                    "encrypted" -> dataset.addLiteral(
                        DVDM.encrypted,
                        model.createTypedLiteral(value, XSDDatatype.XSDboolean)
                    )

                    "anonymised" -> dataset.addLiteral(
                        DVDM.anonymised,
                        model.createTypedLiteral(value, XSDDatatype.XSDboolean)
                    )

                    "useTPM" -> dataset.addLiteral(DVDM.useTPM, model.createTypedLiteral(value, XSDDatatype.XSDboolean))
                    "source" -> dataset.addLiteral(DCTerms.source, value as String)
                }
            }
            setNsPrefixesFiltered(model)
            return Asset(model)
        }

        @JvmOverloads
        fun individualFromRdf(content: String, format: RDFFormat = RDFFormat.RDFXML) =
            Individual.fromRdfString(content, format)

        @JvmOverloads
        fun assetFromRdf(content: String, format: RDFFormat = RDFFormat.RDFXML) =
            Asset.fromRdfString(content, format)

    }
}

internal fun Resource.addStringArray(any: Any, property: Property) = (any as JsonArray)
    .map { it as String }
    .forEach { addLiteral(property, it) }

internal fun Resource.addLongArray(any: Any, property: Property) = (any as JsonArray)
    .map { it as Int }
    .forEach { addLiteral(property, it) }

internal fun Resource.addLangString(any: Any, property: Property) = (any as JsonObject)
    .forEach { (key, value) ->
        addProperty(property, value as String, key)
    }


operator fun Statement.component1(): Resource = subject
operator fun Statement.component2(): Property = predicate
operator fun Statement.component3(): RDFNode = `object`

fun JsonObject.withJsonObject(key: String): JsonObject = getJsonObject(key) ?: JsonObject().apply { this@withJsonObject.put(key, this) }
fun JsonObject.withJsonArray(key: String): JsonArray = getJsonArray(key) ?: JsonArray().apply { this@withJsonArray.put(key, this) }
fun JsonObject.putIfNotNull(key: String, value: Any?): JsonObject = value?.let { put(key, value) } ?: this

fun Resource.rename(uriRef: String): Resource {
    val store = mutableMapOf<Property, String>().apply {
        model.listStatements(this@rename, null, this@rename).let {
            while (it.hasNext()) {
                it.next().apply {
                    it.remove()
                    put(predicate, `object`.asResource().uri)
                }
            }
        }
    }

    return ResourceUtils.renameResource(this, uriRef).apply {
        store.forEach { addProperty(it.key, model.createResource(it.value)) }
    }
}
