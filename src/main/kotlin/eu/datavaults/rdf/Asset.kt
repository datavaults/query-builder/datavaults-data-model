package eu.datavaults.rdf

import eu.datavaults.Licenses
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.RDF
import java.io.StringWriter

class Asset(private val model: Model) {
    @JvmOverloads
    fun asRdfString(format: RDFFormat = RDFFormat.RDFXML_ABBREV) = StringWriter().also {
        RDFDataMgr.write(it, model, format)
    }.toString()

    fun extractJson(): String {
        val json = JsonObject()
        model.listSubjectsWithProperty(RDF.type, DCAT.Dataset).nextOptional().ifPresent { asset ->
            asset.listProperties().forEach { (_, predicate, obj) ->
                when (predicate) {
                    DVDM.category -> json.withJsonArray("category").add(obj.asLiteral().lexicalForm)
                    DCTerms.title -> json.withJsonObject("name").put(obj.asLiteral().language, obj.asLiteral().lexicalForm)
                    DCTerms.description -> json.withJsonObject("description").put(obj.asLiteral().language, obj.asLiteral().lexicalForm)
                    DCAT.keyword -> json.withJsonArray("keywords").add(obj.asLiteral().lexicalForm)
                    DCTerms.license -> json.putIfNotNull("license", Licenses.getConcept(obj.asResource())?.identifier)

                    DVDM.price -> json.put("price", obj.asLiteral().double)

                    DVDM.encrypted -> json.put("encrypted", obj.asLiteral().boolean)
                    DVDM.anonymised -> json.put("anonymised", obj.asLiteral().boolean)
                    DVDM.useTPM -> json.put("useTPM", obj.asLiteral().boolean)

                    DCTerms.source -> json.put("source", obj.asLiteral().lexicalForm)
                }
            }
        }
        return json.encodePrettily()
    }

    companion object {
        @JvmStatic
        @JvmOverloads
        fun fromRdfString(content: String, format: RDFFormat = RDFFormat.RDFXML): Asset {
            val model = ModelFactory.createDefaultModel()
            RDFDataMgr.read(model, content.byteInputStream(), format.lang)
            return Asset(model)
        }
    }
}