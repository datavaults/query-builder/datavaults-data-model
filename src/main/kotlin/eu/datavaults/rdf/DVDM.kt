package eu.datavaults.rdf

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource

object DVDM {

    const val NS = "http://datavaults.eu/ns/dm#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val TabularDataSet: Resource = m.createResource("${NS}TabularDataSet")

    @JvmField
    val category: Property = m.createProperty(NS, "category")

    @JvmField
    val price: Property = m.createProperty(NS, "price")

    @JvmField
    val anonymised: Property = m.createProperty(NS, "anonymised")

    @JvmField
    val encrypted: Property = m.createProperty(NS, "encrypted")

    @JvmField
    val useTPM: Property = m.createProperty(NS, "useTMP")

    @JvmField
    val nationality: Property = m.createProperty(NS, "nationality")

    @JvmField
    val placeOfBirth: Property = m.createProperty(NS, "placeOfBirth")

    @JvmField
    val city: Property = m.createProperty(NS, "city")

    @JvmField
    val region: Property = m.createProperty(NS, "region")

    @JvmField
    val occupation: Property = m.createProperty(NS, "occupation")

    @JvmField
    val disability: Property = m.createProperty(NS, "disability")

    @JvmField
    val qualification: Property = m.createProperty(NS, "qualification")

    @JvmField
    val education: Property = m.createProperty(NS, "education")

    @JvmField
    val culturalInterest: Property = m.createProperty(NS, "culturalInterest")

    @JvmField
    val transportation: Property = m.createProperty(NS, "transportation")

    @JvmField
    val civilStatus: Property = m.createProperty(NS, "civilStatus")

    @JvmField
    val nationalInsuranceNr: Property = m.createProperty(NS, "nationalInsuranceNr")

    @JvmField
    val memberNumber: Property = m.createProperty(NS, "memberNumber")

    @JvmField
    val socialSecurityNumber: Property = m.createProperty(NS, "socialSecurityNumber")

}

