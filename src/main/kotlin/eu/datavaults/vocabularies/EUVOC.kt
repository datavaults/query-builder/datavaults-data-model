package eu.datavaults.vocabularies

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property

object EUVOC {
    const val NS = "http://publications.europa.eu/ontology/euvoc#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val xlNotation: Property = m.createProperty(NS, "xlNotation")
    @JvmField
    val xlCodification: Property = m.createProperty(NS, "xlCodification")
}
