package eu.datavaults.vocabularies

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property

object GEOSPARQL {
    const val NS = "http://www.opengis.net/ont/geosparql#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val sfWithin: Property = m.createProperty(NS, "sfWithin")
}
