package eu.datavaults.vocabularies

import eu.datavaults.rdf.DVDM
import org.apache.jena.rdf.model.*
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.*

@JvmField
val DCATAP_PREFIXES = mapOf(
    "dcat" to DCAT.NS,
    "skos" to SKOS.uri,
    "skosxl" to SKOSXL.uri,
    "foaf" to FOAF.NS,
    "dct" to DCTerms.NS,
    "gsp" to "http://www.opengis.net/ont/geosparql#",
    "odrl" to "https://www.w3.org/TR/odrl-vocab/#",
    "dcatap" to "http://data.europa.eu/r5r/",
    "prov" to "http://www.w3.org/ns/prov#",
    "owl" to OWL.NS,
    "xsd" to XSD.NS,
    "rdfs" to RDFS.uri,
    "gmd" to "http://www.isotc211.org/2005/gmd#",
    "vcard" to VCARD4.NS,
    "adms" to "http://www.w3.org/ns/adms#",
    "spdx" to "http://spdx.org/rdf/terms#",
    "schema" to "http://schema.org/",
    "locn" to "http://www.w3.org/ns/locn#",
    "org" to ORG.NS,
    "time" to "http://www.w3.org/2006/time#",
    "euvoc" to EUVOC.NS,
    "oa" to "http://www.w3.org/ns/oa#",
    "dvdm" to DVDM.NS
)

@JvmField
val DCATAP_PREFIXES_REVERSED = DCATAP_PREFIXES.entries.associate{(k,v) -> v to k}

fun setNsPrefixesFiltered(model: Model) {
    model.clearNsPrefixMap()
    val nsIterator : NsIterator = model.listNameSpaces()
    while (nsIterator.hasNext()) {
        val namespace : String = nsIterator.next()
        DCATAP_PREFIXES_REVERSED[namespace]?.let {
            model.setNsPrefix(it, namespace)
        }
    }
}
