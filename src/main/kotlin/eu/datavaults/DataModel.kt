@file:JvmName("DVDataModel")

package eu.datavaults

import eu.datavaults.vocabularies.EUVOC
import eu.datavaults.vocabularies.GEOSPARQL
import org.apache.jena.query.ParameterizedSparqlString
import org.apache.jena.query.QueryExecutionFactory
import org.apache.jena.query.QueryFactory
import org.apache.jena.query.ResultSetFormatter
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.vocabulary.*
import java.util.*

internal val isoCodesQuery = QueryFactory.create(
    """
        PREFIX euvoc: <${EUVOC.NS}>
        PREFIX dct: <${DCTerms.NS}>
    
        SELECT ?isoCode WHERE {
            ?country euvoc:xlNotation [
                dct:type <http://publications.europa.eu/resource/authority/notation-type/ISO_3166_1_ALPHA_2> ;
                euvoc:xlCodification ?isoCode
            ]
        }
    """.trimIndent()
)

internal val fromIsoCodeQuery = ParameterizedSparqlString(
    """
    PREFIX euvoc: <${EUVOC.NS}>
    PREFIX dct: <${DCTerms.NS}>

    SELECT ?country WHERE {
        ?country euvoc:xlNotation [
            dct:type <http://publications.europa.eu/resource/authority/notation-type/ISO_3166_1_ALPHA_2> ;
            euvoc:xlCodification ?isoCode
        ]
    } LIMIT 1    
""".trimIndent()
)

fun init() {
    Countries.model
    Places.model
}

class Concept(val resource: Resource, val scheme: ConceptScheme) {

    val identifier: String =
        resource.getProperty(DC_11.identifier)?.literal?.string ?: (resource.uri.substringAfterLast("/") ?: "")

    val exactMatch: String = resource.getProperty(SKOS.exactMatch)?.resource?.uri ?: ""

    val prefLabels: Map<String, String>
        get() = resource.listProperties(SKOS.prefLabel).asSequence()
            .associate { it.language to it.literal.lexicalForm }

    val altLabels: Map<String, String>
        get() = resource.listProperties(SKOS.altLabel).asSequence()
            .associate { it.language to it.literal.lexicalForm }

    fun label(lang: String): String? = prefLabel(lang) ?: altLabel(lang)

    fun prefLabel(lang: String): String? = resource.getProperty(SKOS.prefLabel, lang)?.literal?.string
        ?: resource.getProperty(SKOS.prefLabel)?.literal?.lexicalForm

    fun altLabel(lang: String): String? = resource.getProperty(SKOS.altLabel, lang)?.literal?.string
        ?: resource.getProperty(SKOS.altLabel)?.literal?.lexicalForm
}

open class ConceptScheme(resource: String) {

    val model: Model by lazy {
        ModelFactory.createDefaultModel().apply {
            val inputStream = javaClass.classLoader.getResourceAsStream(resource)
            RDFDataMgr.read(this, inputStream, Lang.RDFXML)
        }
    }

    fun isConcept(resource: Resource): Boolean =
        (model.containsResource(resource) && model.getResource(resource.uri).isA(SKOS.Concept))
                || model.listSubjectsWithProperty(OWL.sameAs, resource).nextOptional().isPresent

    fun getConcept(uriRef: String): Concept? = getConcept(model.createResource(uriRef))

    fun getConcept(resource: Resource): Concept? =
        model.listStatements(resource, RDF.type, SKOS.Concept).nextOptional().orElse(null)?.let {
            Concept(it.subject, this)
        } ?: model.listSubjectsWithProperty(OWL.sameAs, resource).nextOptional().orElse(null)?.let {
            Concept(it, this)
        }

    fun fromIdentifier(identifier: String): Concept? = model.listResourcesWithProperty(
        DC_11.identifier,
        identifier
    ).toList().firstOrNull()?.let { Concept(it, this) } ?: model.listResourcesWithProperty(
        SKOS.prefLabel,
        identifier
    ).toList().firstOrNull()?.let { Concept(it, this) }

    @JvmOverloads
    open fun fromLabel(value: String, lang: String = "en"): Concept? =
        model.listSubjectsWithProperty(SKOS.prefLabel, value, lang).nextOptional().orElse(null)?.let {
            Concept(it, this)
        }

}

object Countries : ConceptScheme("countries-skos-ap-act.rdf") {

    @JvmStatic
    val isoCodes: Set<String> by lazy {
        QueryExecutionFactory.create(isoCodesQuery, model).run {
            ResultSetFormatter.toList(execSelect()).map {
                it.getLiteral("isoCode").lexicalForm
            }.toSet()
        }
    }

    @JvmStatic
    val identifiers: Set<String> by lazy {
        model.listObjectsOfProperty(DC.identifier).mapWith { it.asLiteral().lexicalForm }.toSet()
    }

    private val iso31661alpha2Resource =
        model.createResource("http://publications.europa.eu/resource/authority/notation-type/ISO_3166_1_ALPHA_2")

    @JvmStatic
    fun fromIso31661Alpha2Code(isoCode: String): Concept? {
        fromIsoCodeQuery.setLiteral("isoCode", isoCode)
        return QueryExecutionFactory.create(fromIsoCodeQuery.asQuery(), model).run {
            execSelect().next()?.let {
                Countries.getConcept(it.getResource("country"))
            }
        }
    }

    internal fun iso31661alpha2(concept: Concept): String? {
        return concept.resource.listProperties(EUVOC.xlNotation)
            .filterKeep {
                it.`object`.isResource && it.resource.getPropertyResourceValue(DCTerms.type) == iso31661alpha2Resource
            }.mapWith {
                it.getProperty(EUVOC.xlCodification).string
            }.nextOptional().orElse(null)
    }

}

object Places : ConceptScheme("places-skos-ap-act.rdf") {

    @JvmStatic
    val identifiers: Set<String> by lazy {
        model.listObjectsOfProperty(DC.identifier).mapWith { it.asLiteral().lexicalForm }.toSet()
    }

    @JvmStatic
    val names: Set<String> by lazy {
        model.listStatements(null, SKOS.prefLabel, null, "en").mapWith { it.`object`.asLiteral().lexicalForm }.toSet()
    }

    internal fun countryOf(concept: Concept): Concept? =
        concept.resource.getProperty(GEOSPARQL.sfWithin)?.let {
            Countries.getConcept(it.resource)
        }
}

object Licenses: ConceptScheme("licences-skos.rdf") {
    init {
        val inputStream = javaClass.classLoader.getResourceAsStream("piveau-licences-skos.rdf")
        RDFDataMgr.read(model, inputStream, Lang.RDFXML)
    }

    @JvmStatic
    val identifiers: Set<String> by lazy {
        model.listObjectsOfProperty(DC.identifier).mapWith { it.asLiteral().lexicalForm }.toSet()
    }

    @JvmStatic
    val names: Set<String> by lazy {
        model.listStatements(null, SKOS.altLabel, null, "en").mapWith { it.`object`.asLiteral().lexicalForm }.toSet()
    }

    override fun fromLabel(value: String, lang: String): Concept? =
        model.listSubjectsWithProperty(SKOS.altLabel, value, lang).nextOptional().orElse(null)?.let {
            Concept(it, this)
        }
}

internal fun Resource.isA(resource: Resource): Boolean =
    listProperties(RDF.type).toSet().any { it.`object` == resource }

internal fun String.titleCase() = split("\\s+".toRegex()).map { word ->
    word.lowercase().replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
}.joinToString(" ")
