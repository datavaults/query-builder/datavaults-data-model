# DataVaults data model library
Providing convenient helper classes for the DataVaults data model 

## Table of Contents
1. [Build & Install](#build-&-install)
1. [Use](#use)
1. [License](#license)

## Build & Install
Requirements:
* Git
* Maven 3
* Java 17

```bash
$ git clone <gitrepouri>
$ cd datavaults-data-model
$ mvn install
```

## Use

Add repository to your project pom file:
```xml
<repository>
    <id>paca</id>
    <name>paca</name>
    <url>https://paca.fokus.fraunhofer.de/repository/maven-public/</url>
</repository>
```

Add dependency to your project pom file:
```xml
<dependency>
    <groupId>eu.datavaults</groupId>
    <artifactId>datavaults-data-model</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```

## License

[Apache License, Version 2.0](LICENSE.md)
