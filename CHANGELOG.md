# ChangeLog

## 3.0.7 (2023-01-05)

**Added:**
* Add prefixes
* 

## 3.0.6 (2023-01-05)

**Changed:**
* Property names to plural versions

## 3.0.5 (2022-12-02)

**Added:**:
* Possibility to pass the json-ld dataset content as `metadata` property of the json

## 3.0.4 (2022-11-29)

**Changed:**
* Generation of asset just requires a single string for `source`

## 3.0.3 (2022-11-27)

**Changed:**
* `region` decoupled form `Places` vocabulary and stored as string

## 3.0.2 (2022-11-25)

**Changed:**
* `civilStatus` is an array of `Int`
* `qualification` is now an array of `Int`

## 3.0.1 (2022-10-07)

**Fixed:**
* Mapping to `Int` instead of `Long`

## 3.0.0 (2022-10-07)

**Changed:**
* `occupations`, `transportationMeans`, `culturalInterests`, and `disabilities` are now integers

**Added:**
* `education` property (replacing `qualification`)

## 2.1.1 (2022-09-21)

**Added:**
* Title to generated "individual" because title is required for processing

## 2.1.0 (2022-09-20)

**Added:**
* `License` class and `Licenses` object
* Converting RDF to JSON:
  * `DataVaults.individualFromRdf(content, format = RDFXML)`
  * `Individual.fromRdfString(content, format = RDFXML)`
  * `Individual::extractJson()`
  * `DataVaults.assetFromRdf(content, format = RDFXML)`
  * `Asset.fromRdfString(content, format = RDFXML)`
  * `Asset::extractJson()`

**Changed:**
* `price` to float
* Default `asRdfString` format to RDF/XML Abbreviated

## 2.0.0 (2022-09-19)

**Added:**
* `Place` class and `Places` object
* Convenient classes and methods to convert between JSON and RDF

**Changed:**
* List of supported properties

## 1.0.3 (2022-05-25)

**Changed:**
* Convert name and names methods into properties

## 1.0.2 (2022-05-24)

**Changed:**
* Downgrade to Java 11
* Return ISO code capitalized

## 1.0.1 (2022-05-24)

**Fixed:**
* Release tagging

## 1.0.0 (2022-05-24)

Initial release